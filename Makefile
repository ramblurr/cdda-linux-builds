DISTROS = fedora30 debian9 ubuntu18 ubuntu19 archlinux
BUILD_TARGETS = $(patsubst %, build/%, $(DISTROS))
RUN_TARGETS= $(patsubst %, run/%, $(DISTROS))

$(BUILD_TARGETS):
	docker build . -f docker/$(@F)/Dockerfile -t cdda-$(@F)

$(RUN_TARGETS):
	docker volume create data-cdda
	docker run --rm -v data-cdda:/output cdda-$(@F)

collect:
	mkdir -p site
	docker run -d --rm --name dummy -v data-cdda:/output alpine tail -f /dev/null
	docker cp dummy:/output/ site/
	docker stop dummy

clean:
	rm -rf site

publish:
	bash publish.sh > site/index.html
	mv site public
