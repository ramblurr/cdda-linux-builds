#!/usr/bin/env bash

DISTROS="fedora30 debian9 ubuntu18 ubuntu19 archlinux"

DATE=$(date)

cat  << EOF
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CDDA Linux Build Test Results</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/hack/0.8.1/hack.css" integrity="sha256-9Wem8nnlUXfFOzeSuCmPQH9r7yKFrfECZzRuccXu89U=" crossorigin="anonymous" />
  <style>
  .card.failed {
    border: 1px solid #f44336;
}
  .card.failed .card-header {
    color: #f44336;;
}
img {
   width: 100%;
    height: auto;
}
footer.footer {

    border-top: 1px solid #ccc;
    margin-top: 80px;
    margin-top: 5rem;
    padding: 48px 0;
    padding: 3rem 0;

}
</style>
</head>
<body class="mark">
<div class="main container">
<h1>CDDA Automatic Linux Build Testing</h1>
<p class="date">Last Built: $DATE</span>


$(for i in $DISTROS; do
class=$(if [ -f "site/output/$i.png" ]; then echo ""; else echo "failed"; fi;)
job_url=$(cat site/output/$i.url)
echo "<div class="cell -4of12">"
echo "<div class='card $class'>
  <header class='card-header'>$i</header>
  <div class='card-content'>
    <div class='inner'>"
     if [ -f "site/output/$i.png" ]; then
     echo "<img src='output/$i.png'/>"
     echo "<p><a href='output/$i.log' >log</a> <a href='$job_url'>build</a></p>"
  else
     echo failed
     echo "<p><a href='output/$i.log' >log</a> <a href='$job_url'>build</a></p>"
  fi
 echo "</div></div></div>"


done)


</div>
<footer class="footer">
<a href="https://gitlab.com/ramblurr/cdda-linux-builds">source</a>
</footer>
</div>
</body>
</html>
EOF
