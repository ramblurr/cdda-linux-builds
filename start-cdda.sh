#!/bin/bash

sleep 5

echo "Starting CDDA"
echo "$ /Cataclysm-DDA/cataclysm-launcher"

/Cataclysm-DDA/cataclysm-launcher 2>&1 | tee -a /output/${OS}.log & pid_cdda=$!

sleep 10
echo "Taking screenshot: /output/${OS}.png" | tee -a /output/${OS}.log &
import -window root /output/${OS}.png 2>&1 | tee -a /output/${OS}.log &
echo "Cleaning up" | tee -a /output/${OS}.log &
echo $CI_JOB_URL > /output/${OS}.url
kill $pid_cdda
kill -3 $(cat "/var/run/supervisor/supervisord.pid")
